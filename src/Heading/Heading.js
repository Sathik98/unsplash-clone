import React from 'react';
import './Heading.css';
import img0 from '../Images/topimg.jpg';
import logo from '../Images/logo.png';
import logo2 from '../Images/logo2.png';

function Heading() {
    return (

        <>
            <div className="head">
                <div className="mainhead">
                    <img src={logo} alt="logo" />
                </div>
                <img src={img0} alt="mountain" height="600px" width="1536px" />
                <div className="headword">
                    <span>Unsplash</span>
                </div>
                <div className="headpara">
                    <p>The internet’s source of <a href="https://unsplash.com/license">freely-usable images.</a> 
                        Powered by creators everywhere.</p>
                </div>
                <div className="headb1">
                    <h5>Photo of the Day by
                        Greg Rosenke</h5>
                </div>
                <div className="headb2">
                    <h5>Read more about the Unsplash License</h5>
                </div>
                <div className="headb3">
                    <a href="https://www.squarespace.com/website-design?channel=display_nonprogrammatic&subchannel=unsplash&campaign=logocopy-static_1x1&subcampaign=homepage-standard&source=us_homepage-standard&utm_source=ros&utm_medium=display_nonprogrammatic&utm_campaign=2021_us_eng_logocopy-static&utm_term=homepage-standard&utm_content=static">
                    <img src={logo2} alt="squarespace" height="50px" width="200px" /></a>
                    <h5>Create your website today.</h5>
                </div>
            </div>

        </> 
    )
}

export default Heading;