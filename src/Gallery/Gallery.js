import React, { useState } from "react";
import './Gallery.css';
import { BsX } from "react-icons/bs";
import { BsChevronLeft } from "react-icons/bs";
import { BsChevronRight } from "react-icons/bs";
import { BsDownload } from "react-icons/bs";
import { BsPersonCircle } from "react-icons/bs";


const Gallery = () => {

    let images = [
        {
            id: 1,
            imgSrc: 'https://images.pexels.com/photos/719396/pexels-photo-719396.jpeg',
            alt: 'water'
            

        },
        {
            id: 2,
            imgSrc: 'https://cdn.pixabay.com/photo/2018/01/14/23/12/nature-3082832__480.jpg',
            alt: 'tree'
        },
        {
            id: 3,
            imgSrc: 'https://wonderfulengineering.com/wp-content/uploads/2014/10/wallpaper-photos-31.jpg',
            alt: 'bulb'
        },
        {
            id: 4,
            imgSrc: 'https://s3-us-west-2.amazonaws.com/s.cdpn.io/82/flight-formation.jpg',
            alt: 'flight'
        },
        {
            id: 5,
            imgSrc: 'https://images.pexels.com/photos/1591447/pexels-photo-1591447.jpeg',
            alt: 'path'
        },
        {
            id: 6,
            imgSrc: 'https://wallpaperaccess.com/full/472051.jpg',
            alt: 'leaf'
        },
        {
            id: 7,
            imgSrc: 'https://cdn.pixabay.com/photo/2015/04/23/22/00/tree-736885__480.jpg',
            alt: 'sun'
        },
        {
            id: 8,
            imgSrc: 'https://wallpaperaccess.com/full/760289.jpg',
            alt: 'tajmahal'
        },
        {
            id: 9,
            imgSrc: 'https://images.all-free-download.com/images/graphiclarge/hd_balance_pictures_hd_pictures_166628.jpg',
            alt: 'scale'
        },
        {
            id: 10,
            imgSrc: 'https://c4.wallpaperflare.com/wallpaper/974/565/254/windows-11-windows-10-minimalism-hd-wallpaper-thumb.jpg',
            alt: 'windows'
        },
        {
            id: 11,
            imgSrc: 'https://wallpaperaccess.com/full/1209562.jpg',
            alt: 'sparrow'
        },
        {
            id: 12,
            imgSrc: 'http://images6.fanpop.com/image/photos/41000000/4442617-hd-wallpapers-bambidkar-41031077-3840-2160.jpg',
            alt: 'bench'
        },
        {
            id: 13,
            imgSrc: 'https://image.shutterstock.com/image-photo/micro-peacock-feather-hd-imagebest-260nw-1127238584.jpg',
            alt: 'peacock'
        },
        {
            id: 14,
            imgSrc: 'https://newevolutiondesigns.com/images/freebies/hd-widescreen-wallpaper-2.jpg',
            alt: 'groot'
        },
        {
            id: 15,
            imgSrc: 'https://www.iamabiker.com/wp-content/uploads/2021/01/Honda-Highness-CB350-HD-wallpaper-1.jpg',
            alt: 'bike'
        },
        {
            id: 16,
            imgSrc: 'https://cdn.wallpapersafari.com/49/25/vAf7UH.jpg',
            alt: 'cat'
        },
        {
            id: 17,
            imgSrc: 'https://cdn.pixabay.com/photo/2018/08/21/23/29/forest-3622519__340.jpg',
            alt: 'forest'
        },
        {
            id: 18,
            imgSrc: 'https://cdn.pixabay.com/photo/2021/07/06/19/26/drops-6392473__340.jpg',
            alt: 'drops'
        },
        {
            id: 19,
            imgSrc: 'https://p4.wallpaperbetter.com/wallpaper/535/816/24/1920x1080-px-space-stars-people-hot-girls-hd-art-wallpaper-preview.jpg',
            alt: 'space'
        },
        {
            id: 20,
            imgSrc: 'https://en.hdbuzz.net/system/production/media/picture/722/redesign3point0_codev-2020-08-17-6a5e0df6d8-Balance_stones.jpg',
            alt: 'stone'
        },
    ]

    const [model, setModel] = useState(false);

    const [tempimgSrc, setTempImgSrc] = useState('');

    const getImg = (imgSrc) => {
        setTempImgSrc(imgSrc);
        setModel(true);

    }

    return (
        <>
            <div className={model ? "model open" : "model"}>
                <img src={tempimgSrc} alt="tempimg" />
                <BsX onClick={() => setModel(false)} />
                <BsChevronLeft className="lnavi" />
                <BsChevronRight className="rnavi" />
                <BsDownload className="dload" />
                <BsPersonCircle className="author"></BsPersonCircle>

            </div>


            <div className="gallery">
                {images.map((item, index) => {
                    return (
                        <div className="pics" key={index} onClick={() => getImg(item.imgSrc)}>

                            <img src={item.imgSrc} style={{ width: '100%' }} alt="item" />
                            <p className="overlay">Author Name</p>

                        </div>

                    )
                })}

            </div>
        </>
    )

}

export default Gallery;
