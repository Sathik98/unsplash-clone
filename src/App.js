import React from 'react'
import './App.css';
import Heading from './Heading/Heading';
import Gallery from './Gallery/Gallery';



function App() {
  return (
    <div>
      
      
      <Heading></Heading>
      <Gallery></Gallery>

    </div>
  );
}

export default App;
